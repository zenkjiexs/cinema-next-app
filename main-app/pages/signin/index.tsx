import Login from "../../components/Auth/Login";
import PageWrapper from "../../components/Utils/PageWrapper";

const SignInPage = () => {
  return (
    <PageWrapper>
      <Login />
    </PageWrapper>
  );
};

export default SignInPage;
