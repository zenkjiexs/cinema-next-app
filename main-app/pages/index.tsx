import Layout from "@/components/Layout/Layout";
import PageWrapper from "@/components/Utils/PageWrapper";
import { navActions } from "@/redux/reducers/navReducer";
import { useAppDispatch, useAppSelector } from "@/redux/store";
import { moviesConverter } from "@/utils/firebaseConverter/firebaseConverter";
import { useFirestoreQuery } from "@react-query-firebase/firestore";
import {
  collection,
  DocumentData,
  getDocs,
  query,
  QueryDocumentSnapshot,
  SnapshotOptions,
} from "firebase/firestore";
import { db } from "firebaseConfig";
import type {
  GetStaticPropsContext,
  InferGetStaticPropsType,
  NextPageContext,
} from "next";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useEffect, useMemo, useRef } from "react";
import { scroller } from "react-scroll";
import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";
import HeroSlider from "../components/HeroSlider/HeroSlider";
import MovieList from "../components/MovieList/MovieList";

const Home = ({ movies }: InferGetStaticPropsType<typeof getStaticProps>) => {
  const { t } = useTranslation();
  const targetRef = useRef<HTMLDivElement>(null);
  const { scrollToElement } = useAppSelector((state) => state.nav);
  const dispatch = useAppDispatch();
  console.log(movies);
  const nowshowing = useMemo(
    () => movies.filter((movie) => movie.status === "nowshowing"),
    [movies]
  );
  const upcoming = useMemo(
    () => movies.filter((movie) => movie.status === "upcoming"),
    [movies]
  );

  useEffect(() => {
    if (!scrollToElement) return;
    scroller.scrollTo(scrollToElement, {
      duration: 500,
      delay: 0,
      smooth: "easeInOutQuart",
    });
    dispatch(navActions.scrollToElement(undefined));
  }, [scrollToElement, dispatch]);

  return (
    <PageWrapper>
      <Header t={t} targetRef={targetRef} isDynamic={true} />
      <HeroSlider ref={targetRef} movies={nowshowing} />
      <MovieList
        movies={nowshowing}
        name="nowshowing"
        title={t("default:nowshowing")}
        t={t}
      />
      <MovieList
        movies={upcoming}
        name="upcoming"
        title={t("default:upcoming")}
        t={t}
      />
      <Footer />
    </PageWrapper>
  );
};

export default Home;

export const getStaticProps = async (ctx: GetStaticPropsContext) => {
  const moviesRef = collection(db, "movies");
  const moviesQuery = query(moviesRef).withConverter(moviesConverter);
  const moviesQuerySnapshot = await getDocs(moviesQuery);
  const movies = moviesQuerySnapshot.docs.map((doc) => doc.data());
  console.log(movies);
  return {
    props: {
      ...(await serverSideTranslations(ctx.locale as string, [
        "nav",
        "default",
      ])),
      movies,
    },
  };
};
