import Layout from "@/components/Layout/Layout";
import UserProfile from "@/components/UserProfile/UserProfile";
import PageWrapper from "@/components/Utils/PageWrapper";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import React from "react";

export const getStaticProps = async ({ locale }: { locale: string }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["nav", "default"])),
    },
  };
};

const ProfilePage = () => {
  const { t } = useTranslation();
  return (
    <Layout t={t}>
      <UserProfile />
    </Layout>
  );
};

export default ProfilePage;
