import { TFunction } from "next-i18next";
import Image from "next/image";
import React, { FC, RefObject, useEffect, useMemo } from "react";
import NavItem from "./NavItem";
import Search from "/assets/Home/Search.svg";
import useObserveIntersect from "../../hooks/useObserveIntersect";
import { useAuthSignOut, useAuthUser } from "@react-query-firebase/auth";
import { auth, db } from "firebaseConfig";
import UserAvatar from "../Utils/UserAvatar";
import UserIcon from "@/assets/Header/Person.svg";
import LogoutIcon from "@/assets/Header/Logout.svg";
import Link from "next/link";
import { useRouter } from "next/router";
import useAuth from "@/hooks/useAuth";
import { useAppDispatch } from "@/redux/store";
import { navActions } from "@/redux/reducers/navReducer";

type NavItemInfo = {
  name: string;
  title: string;
  tracking: boolean;
};

type Props = {
  t: TFunction;
  targetRef?: RefObject<HTMLDivElement>;
  isDynamic?: boolean;
};

const Header: FC<Props> = ({ t, targetRef, isDynamic = false }) => {
  const user = useAuth({
    staleTime: 5000,
  });

  const isInHeroSection = useObserveIntersect(
    {
      root: null,
      rootMargin: "0px",
      threshold: 0.1,
    },
    targetRef
  );

  const router = useRouter();
  const dispatch = useAppDispatch();

  const navs = useMemo<NavItemInfo[]>(() => {
    return [
      {
        title: t("nav:home"),
        name: "home",
        tracking: true,
      },
      {
        title: t("nav:nowshowing"),
        name: "nowshowing",
        tracking: true,
      },
      {
        title: t("nav:upcoming"),
        name: "upcoming",
        tracking: true,
      },
      {
        title: t("nav:movies"),
        name: "movies",
        tracking: false,
      },
      {
        title: t("nav:cinema"),
        name: "cinema",
        tracking: false,
      },
    ];
  }, [t]);

  const signOutMutation = useAuthSignOut(auth, {
    onSuccess() {
      user.remove();
      router.push("/");
      router.reload();
    },
    onError(error) {
      console.error(error);
    },
  });

  const handleClickLogo = () => {
    if (router.pathname === "/") return;
    router.push("/");
  };

  const handleNavItemClick = (elementName: string) => {
    if (router.pathname === "/") {
      return;
    }
    router.push("/");

    dispatch(navActions.scrollToElement(elementName));
  };
  let headerPosClass = "";
  if (isDynamic) {
    headerPosClass = isInHeroSection
      ? "absolute py-6"
      : "fixed bg-primary-dark py-3";
  } else {
    headerPosClass = "block bg-primary-dark py-3";
  }

  return (
    <div className={`w-full px-24 z-50 ${headerPosClass}`}>
      <div className="w-full h-full flex items-center justify-between">
        <h4
          onClick={handleClickLogo}
          className="font-bold text-3xl mr-12 cursor-pointer select-none"
        >
          Watchflix
        </h4>
        <ul className="flex text-lg gap-12 mr-auto ml-14">
          {navs.map((navItem) => (
            <NavItem
              key={navItem.name}
              title={navItem.title}
              name={navItem.name}
              isDynamic={isDynamic && navItem.tracking}
              onClick={() => handleNavItemClick(navItem.name)}
            />
          ))}
        </ul>
        <div className="flex items-center mr-12 cursor-pointer">
          <Search />
        </div>
        {user.data ? (
          <div className="dropdown dropdown-hover">
            <label tabIndex={0} className="flex items-center">
              <UserAvatar
                photoURL={user.data?.photoURL}
                displayName={user.data?.displayName}
                size={50}
              />
              <div className="flex items-center cursor-pointer">
                <span className="font-bold ml-4 text-base">
                  {user.data?.displayName}
                </span>
              </div>
            </label>
            <ul
              tabIndex={0}
              className={`dropdown-content menu shadow rounded-box min-w-full items-start left-8 ${
                isInHeroSection ? "bg-transparent" : "bg-primary-dark"
              }`}
            >
              <li className="flex items-center pt-2 w-full">
                <Link href="/profile">
                  <a className="min-w-full w-max">
                    <UserIcon width={20} height={20} />
                    <span>Profile</span>
                  </a>
                </Link>
              </li>
              <li
                onClick={() => signOutMutation.mutate()}
                className="flex items-center flex-1 w-full"
              >
                <a className="min-w-full w-max">
                  <LogoutIcon width={20} height={20} />
                  <span>Sign out</span>
                </a>
              </li>
            </ul>
          </div>
        ) : (
          <div className="flex gap-4">
            <Link href="/signin">
              <a className="font-bold text-sm py-1 px-6 border-2 rounded-full">
                Sign in
              </a>
            </Link>
            <Link href="/signup">
              <a className="font-bold text-sm py-1 px-6 border-2 rounded-full">
                Sign up
              </a>
            </Link>
          </div>
        )}
      </div>
    </div>
  );
};

export default Header;
