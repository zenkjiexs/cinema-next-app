import React, { ForwardedRef, forwardRef, useState } from "react";
import CheckIcon from "/assets/Auth/Check.svg";
type Props = {
  className?: string;
};

const Radio = ({ className }: Props, ref: ForwardedRef<HTMLInputElement>) => {
  const [isChecked, setIsChecked] = useState(false);

  const handleClick = () => {
    setIsChecked((prevState) => !prevState);
  };

  return (
    <div
      onClick={handleClick}
      className={`w-6 h-6 rounded-full bg-field-bg p-1 text-gray-light cursor-pointer select-none ${
        className ? className : ""
      }`}
    >
      <CheckIcon className={`${isChecked ? "visible" : "hidden"}`}></CheckIcon>
      <input
        type="checkbox"
        ref={ref}
        defaultChecked={isChecked}
        className="hidden"
      />
    </div>
  );
};

export default forwardRef(Radio);
