import React, { useEffect } from "react";
import GoogleIcon from "/assets/Auth/Google.svg";
import FacebookIcon from "/assets/Auth/Facebook.svg";
import InputField from "../Utils/InputField";
import Image from "next/image";
import BackgroundImg from "/assets/Auth/Background2.jpg";
import { SubmitHandler, useForm } from "react-hook-form";
import { AuthFormValues } from "../../types/form";
import yup from "../../utils/validation/yupGlobal";
import { yupResolver } from "@hookform/resolvers/yup";
import {
  useAuthCreateUserWithEmailAndPassword,
  useAuthSignInWithPopup,
  useAuthSignInWithRedirect,
  useAuthUpdateProfile,
} from "@react-query-firebase/auth";
import { auth, googleProvider, facebookProvider, db } from "firebaseConfig";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { toastOptions } from "@/utils/toastConfig";
import { useRouter } from "next/router";
import { createUserIfNotExists } from "@/utils/firebase";

const LoginSchema = yup.object({
  fullName: yup
    .string()
    .required("Please enter your full name")
    .matches(/^[aA-zZ\s]+$/, "Please enter valid name")
    .max(50, "Must be less than 50 characters"),

  email: yup
    .string()
    .required("Please enter a valid email")
    .email("Please enter a valid email"),

  password: yup
    .string()
    .required("Password is required")
    .min(8, "Password must be at least 8 characters")
    .max(64, "Password must be less than 64 characters")
    .password("Your password must contains at least one letter and one number"),
});

const Register = () => {
  const {
    register,
    handleSubmit,
    resetField,
    watch,
    formState: { errors },
  } = useForm<AuthFormValues>({
    mode: "onTouched",
    resolver: yupResolver(LoginSchema),
  });

  const router = useRouter();
  const updateProfileMutation = useAuthUpdateProfile({
    onSuccess: () => {
      router.push("/");
    },
  });
  const signUpMutation = useAuthCreateUserWithEmailAndPassword(auth, {
    onError(error) {
      if (error.code === "auth/email-already-in-use") {
        toast.error("This email address already being use", toastOptions);
      } else {
        toast.error(error.message, toastOptions);
      }
      resetField("password");
    },
    onSuccess(data) {
      updateProfileMutation.mutate({
        user: data.user,
        displayName: watch("fullName"),
      });
      createUserIfNotExists(data.user);
    },
  });

  const onSubmit: SubmitHandler<AuthFormValues> = (data) => {
    const { email, password } = data;
    signUpMutation.mutate({
      email,
      password,
    });
  };

  const authMutation = useAuthSignInWithPopup(auth, {
    onError(error) {
      if (error.code === "auth/popup-closed-by-user") {
        toast.error("Sign in cancelled", toastOptions);
      } else {
        toast.error(error.message, toastOptions);
      }
    },
    onSuccess(data) {
      toast.success("Sign in successful", toastOptions);
      createUserIfNotExists(data.user);
      router.push("/");
    },
  });

  function onSignInWithGoogle() {
    authMutation.mutate({
      provider: googleProvider,
    });
  }

  const onSignInWithFacebook = () => {
    authMutation.mutate({
      provider: facebookProvider,
    });
  };

  const handleClickSignIn = () => {
    router.push("/signin");
  };

  const handleClickLogo = () => {
    router.push("/");
  };

  // useEffect(() => {
  //   getRedirectResult(auth)
  //     .then((result) => {
  //       if (result?.user) {
  //         router.push("/");
  //       }
  //     })
  //     .catch((err) => {
  //       if (err.code === "auth/account-exists-with-different-credential") {
  //         toast.error(
  //           "There are already an account associated with this email",
  //           toastOptions
  //         );
  //       } else {
  //         toast.error(err.message, toastOptions);
  //       }
  //     });
  // }, [router]);

  return (
    <>
      <ToastContainer />
      <div className="w-full min-h-screen h-full grid grid-cols-[1fr_max-content] relative">
        <div className="absolute left-10 top-4 z-10">
          <h1
            onClick={handleClickLogo}
            className="font-bold cursor-pointer text-[40px]"
          >
            Watchflix
          </h1>
        </div>

        <div className="w-full h-full relative">
          <h2 className="absolute z-20 text-6xl font-bold translate-x-[-50%] translate-y-[-50%] left-1/2 top-1/2 w-3/4 text-center">
            Discover new movies and buy tickets.
          </h2>
          <Image
            src={BackgroundImg}
            alt="Movie's cover"
            layout="fill"
            objectFit="cover"
          />
          <div className="absolute w-full h-full bg-[#00000049]"></div>
        </div>

        <div className="bg-[#161616] p-16 flex flex-col items-center justify-center z-20  max-w-[650px] w-full">
          <h1 className="font-bold text-3xl">Welcome to Watchflix</h1>
          <span className="text-gray-light font-medium mt-2">
            &#8212; SIGN UP WITH &#8212;
          </span>
          <div className="grid grid-cols-2 gap-2 mt-6">
            <div
              onClick={() => onSignInWithGoogle()}
              className="border-radiants before:bg-auth-btn before:rounded-lg py-3 px-5 flex items-center gap-4 cursor-pointer"
            >
              <GoogleIcon width="24" height="24" />
              <span>Sign Up with Google</span>
            </div>
            <div
              onClick={() => onSignInWithFacebook()}
              className="border-radiants before:bg-auth-btn before:rounded-lg py-3 px-5 flex items-center gap-4 cursor-pointer"
            >
              <FacebookIcon width="24" height="24" />
              <span>Sign Up with Facebook</span>
            </div>
          </div>
          <span className="text-gray-light font-medium mt-6">
            &#8212; OR &#8212;
          </span>
          <form
            className="mt-4 flex flex-col self-stretch gap-4"
            onSubmit={handleSubmit(onSubmit)}
          >
            <div className="flex flex-col gap-3">
              <InputField<AuthFormValues>
                label="Full Name"
                name="fullName"
                register={register}
                error={errors.fullName?.message}
              />
              <InputField<AuthFormValues>
                label="Email"
                name="email"
                register={register}
                error={errors.email?.message}
              />
              <InputField<AuthFormValues>
                label="Password"
                name="password"
                type="password"
                register={register}
                error={errors.password?.message}
              />
            </div>

            <button
              type="submit"
              className="bg-auth-btn py-3 rounded-lg font-medium text-lg mt-5 select-none"
            >
              Create an account
            </button>
            <p className="mx-auto text-gray-light mt-5">
              Already have an account?{" "}
              <span
                onClick={handleClickSignIn}
                className="underline text-typo-light font-bold cursor-pointer"
              >
                Sign In Here
              </span>
            </p>
          </form>
        </div>
      </div>
    </>
  );
};

export default Register;
