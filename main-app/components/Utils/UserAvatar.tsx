import Image from "next/image";
import React, { memo, useState } from "react";
import Avatar from "react-avatar";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

type Props = {
  photoURL: string | null | undefined;
  displayName: string | null | undefined;
  size?: number;
  className?: string;
};
const UserAvatar = ({ photoURL, displayName, className, size = 50 }: Props) => {
  const [isLoad, setIsLoad] = useState(false);

  const skeleton = (
    <SkeletonTheme baseColor="#777777" highlightColor="#444">
      <Skeleton height={size} circle={true} />
    </SkeletonTheme>
  );

  let avatar = skeleton;

  if (photoURL) {
    avatar = (
      <>
        {!isLoad && skeleton}
        <Image
          src={photoURL}
          alt="Avatar"
          layout="fill"
          objectFit="cover"
          onLoadingComplete={() => setIsLoad(true)}
        />
      </>
    );
  }
  if (!photoURL && displayName) {
    avatar = <Avatar name={displayName || "No name"} size={size.toString()} />;
  }

  return (
    <div
      style={{ width: size, height: size }}
      className={`relative rounded-full overflow-hidden ${
        className ? className : ""
      }`}
    >
      {avatar}
    </div>
  );
};

export default memo(UserAvatar);
