import React, { FC } from "react";

const PageWrapper: FC = ({ children }) => {
  return (
    <div className="max-w-[1660px] mx-auto relative text-typo-light bg-secondary-dark">
      {children}
    </div>
  );
};

export default PageWrapper;
