import React, { InputHTMLAttributes, useEffect, useRef, useState } from "react";
import { Path, RegisterOptions, UseFormRegister } from "react-hook-form";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

export type FormInputProps<TFormValues> = {
  name: Path<TFormValues>;
  register: UseFormRegister<TFormValues>;
  rules?: RegisterOptions;
  background?: string;
  isLoading?: boolean;
  error?: string;
  label: string;
  floatingLabel?: boolean;
  labelColor?: string;
} & Omit<InputHTMLAttributes<HTMLInputElement>, "name">;

const InputField = <TFormValues extends Record<string, unknown>>({
  label,
  floatingLabel = false,
  name,
  error,
  className,
  register,
  background,
  labelColor,
  isLoading = false,
  ...rest
}: FormInputProps<TFormValues>): JSX.Element => {
  const [hasValue, setHasValue] = useState(false);
  const { ref, onBlur, ...registerProps } = { ...register(name) };
  const inputRef = useRef<HTMLInputElement | null>(null);
  const handleBlur = () => {
    if (inputRef.current?.value) {
      setHasValue(true);
    } else {
      setHasValue(false);
    }
  };

  if (isLoading) {
    return (
      <SkeletonTheme baseColor="#202020" highlightColor="#444">
        <Skeleton height={50} />
      </SkeletonTheme>
    );
  }

  return (
    <div className={`${className ? className : ""}`}>
      <div
        className={`relative bg-field-bg px-6 pt-4 rounded-lg ${
          background ? background : ""
        }`}
      >
        <input
          className="bg-transparent outline-none peer py-2 w-full"
          ref={(e) => {
            ref(e);
            inputRef.current = e;
          }}
          onBlur={(e) => {
            onBlur(e);
            handleBlur();
          }}
          {...registerProps}
          {...rest}
        />
        <label
          className={`absolute pointer-events-none left-6 font-semibold  transition-all origin-top-left peer-focus:translate-y-[-10px] peer-focus:scale-75 peer-autofill:translate-y-[-10px] peer-autofill:scale-75 ${
            floatingLabel || hasValue ? "translate-y-[-10px] scale-75" : ""
          } ${labelColor ? labelColor : "text-gray-light"}`}
        >
          {label}
        </label>
      </div>
      {error && (
        <div className="ml-1 mt-[6px] text-[#e65c00] text-sm leading-4">
          {error}
        </div>
      )}
    </div>
  );
};

export default InputField;
