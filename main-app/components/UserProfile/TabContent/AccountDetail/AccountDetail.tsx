import React, { useCallback, useEffect, useMemo, useState } from "react";
import yup from "@/utils/validation/yupGlobal";
import { yupResolver } from "@hookform/resolvers/yup";
import { SubmitHandler, useForm } from "react-hook-form";
import { ProfileForm } from "@/types/form";
import "yup-phone";
import InputField from "../../../Utils/InputField";
import CaptureIcon from "@/assets/Profile/Capture.svg";

import UserAvatar from "@/components/Utils/UserAvatar";
import useAuth from "@/hooks/useAuth";
import useUpdateUser from "@/hooks/useUpdateUser";
import { auth } from "firebaseConfig";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { toastOptions } from "@/utils/toastConfig";
import EditBtn from "../EditBtn";
import UploadAvatar from "./UploadAvatar";

const AccountSchema = yup.object({
  fullName: yup
    .string()
    .required("Please enter your full name")
    .matches(/^[aA-zZ\s]+$/, "Please enter valid name")
    .max(50, "Must be less than 50 characters"),

  email: yup
    .string()
    .required("Please enter a valid email")
    .email("Please enter a valid email"),

  address: yup.string().required("Please enter your address"),

  phone: yup
    .string()
    .required("Please enter your phone number")
    .phone("VN", false, "Must be a valid phone number"),
});
const AccountDetail = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    setFocus,
  } = useForm<ProfileForm>({
    mode: "onBlur",
    resolver: yupResolver(AccountSchema),
  });
  const [isUploading, setIsUploading] = useState(false);
  const [userInfo, setUserInfo] = useState<ProfileForm | undefined>();
  const user = useAuth();

  const updateProfileMutation = useUpdateUser(auth, {
    onSuccess: () => {
      toast.success("Update profile successfully", toastOptions);
      user.refetch();
      setIsEdit(false);
    },
    onError: () => {
      toast.error("Update profile failed", toastOptions);
      setIsEdit(false);
    },
  });

  const [isEdit, setIsEdit] = useState(false);

  const onSubmit: SubmitHandler<ProfileForm> = (data) => {
    updateProfileMutation.mutate({
      displayName: data.fullName,
      address: data.address,
      phone: data.phone,
    });
  };

  const handleEdit = useCallback(() => {
    setIsEdit(true);
    setTimeout(() => setFocus("fullName"), 0);
  }, [setFocus]);

  const handleOpenUploading = () => {
    setIsUploading(true);
    setIsEdit(false);
  };

  const isSubmit = updateProfileMutation.isLoading;
  const handleReset = useCallback(() => {
    if (isSubmit) return;
    setIsEdit(false);
    reset(userInfo);
  }, [reset, setIsEdit, isSubmit, userInfo]);

  const handleUploadImageSuccess = () => {
    user.refetch();
  };

  useEffect(() => {
    if (user.data) {
      const userInfo = {
        fullName: user.data.displayName || "",
        email: user.data.email || "",
        address: user.data.address || "",
        phone: user.data.phone || "",
      };
      setUserInfo(userInfo);
      reset(userInfo);
    }
  }, [user.data, reset]);

  return (
    <>
      <ToastContainer />
      <div className="flex flex-col gap-12 flex-1">
        <div className="flex gap-6 items-center">
          <UserAvatar
            photoURL={user.data?.photoURL}
            displayName={user.data?.displayName}
          />
          <button
            onClick={handleOpenUploading}
            className="btn btn-sm flex items-center gap-2 hover:bg-secondary-yellow bg-secondary-yellow text-title-dark"
          >
            <CaptureIcon width={20} height={20} />

            <span className="mt-[2px]">Upload new picture</span>
          </button>
          {isUploading && (
            <UploadAvatar
              onClose={() => setIsUploading(false)}
              onUploadSuccess={handleUploadImageSuccess}
            />
          )}
        </div>

        <form className="flex flex-col gap-4" onSubmit={handleSubmit(onSubmit)}>
          <InputField<ProfileForm>
            label="Email"
            name="email"
            register={register}
            background="bg-gray-dark"
            disabled={true}
            floatingLabel={true}
            isLoading={user.isLoading}
          />
          <InputField<ProfileForm>
            label="Full Name"
            name="fullName"
            register={register}
            error={errors.fullName?.message}
            background="bg-gray-dark"
            disabled={!isEdit}
            floatingLabel={true}
            isLoading={user.isLoading}
          />
          <InputField<ProfileForm>
            label="Your Address"
            name="address"
            register={register}
            error={errors.address?.message}
            autoComplete="address"
            defaultValue={undefined}
            background="bg-gray-dark"
            disabled={!isEdit}
            floatingLabel={!!user.data?.address}
            isLoading={user.isLoading}
          />
          <InputField<ProfileForm>
            label="Phone"
            name="phone"
            register={register}
            error={errors.phone?.message}
            autoComplete="tel"
            defaultValue={undefined}
            background="bg-gray-dark"
            disabled={!isEdit}
            floatingLabel={!!user.data?.phone}
            isLoading={user.isLoading}
          />

          <EditBtn
            isEdit={isEdit}
            onEdit={handleEdit}
            onReset={handleReset}
            isSubmit={isSubmit}
          />
        </form>
      </div>
    </>
  );
};

export default AccountDetail;
