import { Tab } from "@/types/profile";
import { User } from "firebase/auth";
import React, { memo } from "react";
import Skeleton from "react-loading-skeleton";
import AccountDetail from "./AccountDetail/AccountDetail";
import ChangePassword from "./ChangePassword";
import TransactionHistory from "./TransactionHistory";

type Props = {
  selectedTab: Tab;
};
const TabContent = ({ selectedTab: tab }: Props) => {
  return (
    <>
      {tab.code === "DETAIL" && <AccountDetail />}
      {tab.code === "PASSWORD" && <ChangePassword />}
      {tab.code === "TRANSACTION" && <TransactionHistory />}
    </>
  );
};

export default memo(TabContent);
