import { yupResolver } from "@hookform/resolvers/yup";
import React from "react";
import { useForm } from "react-hook-form";
import { ChangePasswordForm } from "@/types/form";
import yup from "../../../utils/validation/yupGlobal";
import InputField from "../../Utils/InputField";

const ChangePasswordSchema = yup.object({
  oldPassword: yup.string().required("Please enter your old password"),
  newPassword: yup
    .string()
    .required("Password is required")
    .min(8, "Password must be at least 8 characters")
    .max(64, "Password must be less than 64 characters")
    .password("Your password must contains at least one letter and one number"),
  confirmPassword: yup
    .string()
    .required("Confirm password is required")
    .oneOf([yup.ref("newPassword"), null], "Passwords must match"),
});
const ChangePassword = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<ChangePasswordForm>({
    mode: "onBlur",
    resolver: yupResolver(ChangePasswordSchema),
  });
  return (
    <div className="flex flex-col gap-4 flex-1">
      <InputField<ChangePasswordForm>
        label="Current Password"
        name="curPassword"
        type="password"
        register={register}
        error={errors.curPassword?.message}
        autoComplete="password"
        background="bg-gray-dark"
      />
      <InputField<ChangePasswordForm>
        label="New Password"
        name="newPassword"
        type="password"
        register={register}
        error={errors.newPassword?.message}
        background="bg-gray-dark"
      />
      <InputField<ChangePasswordForm>
        label="Confirm Password"
        name="confirmPassword"
        type="password"
        register={register}
        error={errors.confirmPassword?.message}
        background="bg-gray-dark"
      />
      <div className="flex justify-end gap-2 mt-6">
        <button className="btn btn-md hover:bg-secondary-yellow bg-secondary-yellow text-title-dark flex gap-2">
          <span>Change Password</span>
        </button>
      </div>
    </div>
  );
};

export default ChangePassword;
