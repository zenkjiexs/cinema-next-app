import { MovieStatus } from "@/types/movie";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import YourRating from "./YourRating";
import StarFull from "/assets/MovieDetail/StarFull.svg";

interface Props {
  id: string;
  title: string;
  duration: string;
  rating: number;
  ageRating: string;
  releaseDate: string;
  votes: number;
  yourRating: number | undefined;
  status: MovieStatus;
}

const MovieDetailHeader = (props: Props) => {
  const router = useRouter();

  const handleBook = (movieId: string) => {
    // append booking to current url
    router.push(`/movies/${movieId}/booking`);
  };
  return (
    <div className="flex justify-between items-center">
      <div className="flex flex-col gap-2">
        <h2 className="text-5xl">{props.title}</h2>
        <ul className="flex text-text-infield">
          <li>{props.releaseDate}</li>
          <li className="before:inline-block before:align-middle before:p-[1px] before:bg-current before:mx-2 mb-1">
            {props.ageRating}
          </li>
          <li className="before:inline-block before:align-middle before:p-[1px] before:bg-current before:mx-2 mb-1">
            {props.duration}
          </li>
        </ul>
      </div>
      <div className="flex text-text-infield gap-3">
        {props.status === "nowshowing" && (
          <button
            onClick={() => handleBook(props.id)}
            className="btn text-primary-dark bg-secondary-yellow self-center md:hover:bg-yellow-300 "
          >
            Buy Ticket
          </button>
        )}
        <div className="flex flex-col items-center gap-1">
          <h4 className="uppercase text-sm font-semibold tracking-wider">
            Rating
          </h4>
          <div className=" w-max flex items-center px-4 py-1 gap-2 cursor-pointer md:hover:bg-title-dark rounded-lg">
            <div className="w-6 h-6 relative">
              <StarFull />
            </div>
            <div className="flex flex-col">
              <div className="flex items-center">
                <span className="text-xl mr-[2px] font-bold text-typo-light leading-none">
                  {props.rating}
                </span>
                <span className="tracking-widest mb-[2px] leading-none">
                  /5
                </span>
              </div>
              <span className="text-sm leading-none">{props.votes} votes</span>
            </div>
          </div>
        </div>
        <div className="flex flex-col items-center gap-1">
          <h4 className="uppercase text-sm font-semibold tracking-wider">
            Your Rating
          </h4>
          <YourRating rating={props.yourRating} />
        </div>
      </div>
    </div>
  );
};

export default MovieDetailHeader;
