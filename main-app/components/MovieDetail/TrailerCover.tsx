import Image from "next/image";
import React, { FC, ForwardedRef, forwardRef } from "react";

type WrapperProps = {
  children: React.ReactNode;
};

const TrailerCover = (url: string) => {
  const wrapper = (
    { children }: WrapperProps,
    ref: ForwardedRef<WrapperProps>
  ) => {
    return (
      <div className="relative w-full h-full">
        <Image
          src={url}
          alt={"Trailer cover"}
          layout="fill"
          objectFit="cover"
          priority={true}
        />
        {children}
      </div>
    );
  };
  return forwardRef(wrapper);
};

export default TrailerCover;
