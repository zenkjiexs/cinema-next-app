import Image from "next/image";
import React from "react";
import StarOutlined from "/assets/MovieDetail/StarOutlined.svg";
import StarFullBlue from "/assets/MovieDetail/StarFullBlue.svg";
type Props = {
  rating: number | undefined;
  className?: string;
};

const YourRating = ({ rating, className }: Props) => {
  return (
    <div
      className={`flex items-center px-4 py-1 cursor-pointer md:hover:bg-title-dark rounded-lg flex-1 ${
        className || ""
      }`}
    >
      {rating ? (
        <>
          <div className="relative mr-2">
            <StarOutlined fill="#5799EF" width="24" height="24" />
          </div>
          <span className="text-xl mr-[2px] font-bold text-typo-light leading-none">
            {rating}
          </span>
          <span className="text-[17px] tracking-widest mt-[2px] leading-none">
            /5
          </span>
        </>
      ) : (
        <>
          <div className="relative mr-2">
            <StarOutlined width="24" height="24" />
          </div>
          <span className="text-xl font-bold text-[#5799EF]">Rate</span>
        </>
      )}
    </div>
  );
};

export default YourRating;
