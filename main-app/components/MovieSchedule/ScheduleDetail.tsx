import { bookingActions } from "@/redux/reducers/bookingReducer";
import { useAppDispatch } from "@/redux/store";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import {
  formatTimeString,
  getDistanceFromLatLonInKm,
  groupBy,
  isMobile,
} from "../../utils/helpers";
import useAuth from "@/hooks/useAuth";
import { Cinema, Schedule } from "@/types/movieBooking";

interface Coordinates {
  latitude: number;
  longitude: number;
}

type Region = {
  name: string;
  id: string;
};

interface MovieFormat {
  id: string;
  name: string;
}

interface scheduleDetail {
  movieFormat: MovieFormat;
  startTimes: string[];
}

interface DailySchedule {
  date: string; // dd/mm/yyyy
  scheduleDetail: scheduleDetail[] | undefined;
}

interface CinemaMovie {
  id: string;
  name: string;
  location: Coordinates;
  movieId: string;
  movieSchedule: DailySchedule[];
}

const scheduleDates = [
  "2022-05-21",
  "2022-05-22",
  "2022-05-23",
  "2022-05-24",
  "2022-05-25",
  "2022-05-26",
  "2022-05-27",
  "2022-05-28",
  "2022-05-29",
  "2022-05-30",
  "2022-05-31",
];

const data: CinemaMovie[] = [
  {
    id: "1",
    name: "CGV Hồ Gươm Plaza",
    location: {
      latitude: 20.978772,
      longitude: 105.785378,
    },
    movieId: "1",
    movieSchedule: [
      {
        date: "2022-05-21",
        scheduleDetail: [
          {
            movieFormat: {
              id: "2d",
              name: "2D Vietsub",
            },
            startTimes: ["10:00", "12:00", "14:00", "16:00", "18:00"],
          },
        ],
      },
      {
        date: "2022-05-22",
        scheduleDetail: [
          {
            movieFormat: {
              id: "2d",
              name: "2D Vietsub",
            },
            startTimes: ["10:00", "12:00", "14:00", "16:00", "18:00"],
          },
        ],
      },
    ],
  },
  {
    id: "2",
    name: "CGV Machinco",
    location: {
      latitude: 20.983392,
      longitude: 105.791462,
    },
    movieId: "1",
    movieSchedule: [
      {
        date: "2022-05-21",
        scheduleDetail: [
          {
            movieFormat: {
              id: "2d",
              name: "2D Vietsub",
            },
            startTimes: ["10:00", "12:00", "14:00", "16:00", "18:00"],
          },
        ],
      },
      {
        date: "2022-05-22",
        scheduleDetail: [
          {
            movieFormat: {
              id: "2d",
              name: "2D Vietsub",
            },
            startTimes: ["10:00", "12:00", "14:00", "16:00", "18:00"],
          },
        ],
      },
      {
        date: "2022-05-23",
        scheduleDetail: [
          {
            movieFormat: {
              id: "2d",
              name: "2D Vietsub",
            },
            startTimes: ["10:00", "12:00", "14:00", "16:00", "18:00"],
          },
          {
            movieFormat: {
              id: "imax2d",
              name: "IMAX 2D",
            },
            startTimes: ["10:00", "12:00", "14:00", "16:00", "18:00"],
          },
        ],
      },
    ],
  },
];

let isMobileDevice = false;

type Props = {
  coors: Coordinates | undefined;
  data: Schedule;
  cinemas: Cinema[];
  date: number;
};

type BookingPayload = {
  time: number;
  cinemaId: string;
  type: string;
  room: string;
  roomShiftPath: string;
  endTime: number;
  cinemaName: string;
};
const ScheduleDetail = ({ coors, data, cinemas, date }: Props) => {
  useEffect(() => {
    isMobileDevice = isMobile();
  }, []);
  const router = useRouter();

  const user = useAuth();
  console.log(`user`, user);

  const dispatch = useAppDispatch();

  const bookingHandler = ({
    time,
    cinemaId,
    type,
    room,
    roomShiftPath,
    endTime,
    cinemaName,
  }: BookingPayload) => {
    if (!user.data) {
      router.push("/signin");
      return;
    }
    dispatch(
      bookingActions.booking({
        time,
        cinemaId,
        type,
        room,
        roomShiftPath,
        endTime,
        cinemaName,
      })
    );
    // append current url to booking
    router.push(`${router.asPath}/booking`);
  };

  return (
    <div className="flex flex-col gap-4">
      {data.cinemaSchedules?.map((cinemaSchedule, idx) => {
        const cinema = cinemas.find(
          (cinema) => cinema.id === cinemaSchedule.id
        );
        const cinemaLocation = cinema?.location.split(",") || [
          "21.0278",
          "105.8342",
        ];
        const cinemaCoors: Coordinates = {
          latitude: parseFloat(cinemaLocation[0]),
          longitude: parseFloat(cinemaLocation[1]),
        };
        const hourSchedulesGroupByShowType = groupBy(
          cinemaSchedule.hourSchedules,
          (i) => i.showType
        );
        console.log(hourSchedulesGroupByShowType);
        return (
          <div
            key={idx}
            className="flex flex-col gap-4 bg-gray-dark text-typo-light py-6 px-8 rounded-lg"
          >
            <div className="flex gap-4 items-center">
              <h3 className="text-xl font-medium leading-none">
                {cinemaSchedule.name}
              </h3>
              {isMobileDevice && coors && (
                <div className="font-medium mt-[2px]">
                  {`~ ${getDistanceFromLatLonInKm(cinemaCoors, coors).toFixed(
                    2
                  )} km away`}
                </div>
              )}
            </div>
            <div className="flex flex-col gap-4">
              {cinemaSchedule.hourSchedules.length > 0 ? (
                Object.entries(hourSchedulesGroupByShowType).map(
                  ([key, value], idx) => {
                    value.sort((a, b) => {
                      return a.startTimestamp > b.startTimestamp ? 1 : -1;
                    });
                    return (
                      <div className="flex flex-col gap-2" key={idx}>
                        <span className="text-base uppercase">{key}</span>
                        <div className="flex gap-4 items-center flex-wrap">
                          {value.map((shift, idx) => {
                            const startTime = new Date(shift.startTimestamp);
                            const now = new Date();
                            const isTimePassed = startTime <= now;

                            return (
                              <button
                                onClick={() =>
                                  bookingHandler({
                                    time: shift.startTimestamp,
                                    cinemaId: cinema?.id || "",
                                    type: shift.showType,
                                    room: shift.room,
                                    roomShiftPath: shift.roomShiftPath,
                                    endTime: shift.endTimestamp,
                                    cinemaName: cinema?.name || "",
                                  })
                                }
                                key={idx}
                                className={`py-[2px] font-medium border-2 rounded-md w-[90px] ${
                                  isTimePassed
                                    ? "cursor-default bg-gray-light text-gray-dark border-gray-light"
                                    : "cursor-pointer hover:text-gray-dark hover:bg-white border-white"
                                }`}
                                disabled={isTimePassed}
                              >
                                {formatTimeString(startTime)}
                              </button>
                            );
                          })}
                        </div>
                      </div>
                    );
                  }
                )
              ) : (
                <p className="text-[17px] text-gray-light">
                  No schedule available
                </p>
              )}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default ScheduleDetail;
