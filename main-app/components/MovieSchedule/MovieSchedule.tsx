import React, { useEffect, useState } from "react";
import SelectLocation from "./SelectLocation";
import LocationIcon from "/assets/MovieDetail/Location.svg";
import useLocation from "../../hooks/useLocation";
import { toast, ToastContainer } from "react-toastify";
import { toastOptions } from "@/utils/toastConfig";
import "react-toastify/dist/ReactToastify.css";
import LoadingButton from "../Utils/LoadingButton";
import ScheduleButton from "./ScheduleButton";
import { getDistanceFromLatLonInKm, isMobile } from "../../utils/helpers";
import ScheduleDetail from "./ScheduleDetail";
import { useQuery } from "react-query";
import { Schedule } from "@/types/movieBooking";
import {
  useFirestoreDocumentData,
  useFirestoreQuery,
  useFirestoreQueryData,
} from "@react-query-firebase/firestore";
import { collection, limit, query, where } from "firebase/firestore";
import { db } from "firebaseConfig";
import {
  cinemaConverter,
  scheduleConverter,
} from "@/utils/firebaseConverter/firebaseConverter";

type Region = {
  name: string;
  id: string;
};

interface Coordinates {
  latitude: number;
  longitude: number;
}

interface MovieFormat {
  id: string;
  name: string;
}

interface scheduleDetail {
  movieFormat: MovieFormat;
  startTimes: string[];
}

interface DailySchedule {
  date: string; // dd/mm/yyyy
  scheduleDetail: scheduleDetail[] | undefined;
}

interface CinemaMovie {
  id: string;
  name: string;
  location: Coordinates;
  movieId: string;
  movieSchedule: DailySchedule[];
}
// generate timestamps for 12 days
const generateTimestamps = () => {
  const timestamps = [];

  for (let i = 0; i < 12; i++) {
    const date = new Date();
    date.setDate(date.getDate() + i);
    const timestamp = new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate()
    ).getTime();
    timestamps.push(timestamp);
  }
  return timestamps;
};

const scheduleDates = generateTimestamps();

const data: CinemaMovie[] = [
  {
    id: "1",
    name: "CGV Hồ Gươm Plaza",
    location: {
      latitude: 20.978772,
      longitude: 105.785378,
    },
    movieId: "1",
    movieSchedule: [
      {
        date: "2022-05-21",
        scheduleDetail: [
          {
            movieFormat: {
              id: "2d",
              name: "2D Vietsub",
            },
            startTimes: ["10:00", "12:00", "14:00", "16:00", "18:00"],
          },
        ],
      },
      {
        date: "2022-05-22",
        scheduleDetail: [
          {
            movieFormat: {
              id: "2d",
              name: "2D Vietsub",
            },
            startTimes: ["10:00", "12:00", "14:00", "16:00", "18:00"],
          },
        ],
      },
    ],
  },
  {
    id: "2",
    name: "CGV Machinco",
    location: {
      latitude: 20.983392,
      longitude: 105.791462,
    },
    movieId: "1",
    movieSchedule: [
      {
        date: "2022-05-21",
        scheduleDetail: [
          {
            movieFormat: {
              id: "2d",
              name: "2D Vietsub",
            },
            startTimes: ["10:00", "12:00", "14:00", "16:00", "18:00"],
          },
        ],
      },
      {
        date: "2022-05-22",
        scheduleDetail: [
          {
            movieFormat: {
              id: "2d",
              name: "2D Vietsub",
            },
            startTimes: ["10:00", "12:00", "14:00", "16:00", "18:00"],
          },
        ],
      },
      {
        date: "2022-05-23",
        scheduleDetail: [
          {
            movieFormat: {
              id: "2d",
              name: "2D Vietsub",
            },
            startTimes: ["10:00", "12:00", "14:00", "16:00", "18:00"],
          },
          {
            movieFormat: {
              id: "imax2d",
              name: "IMAX 2D",
            },
            startTimes: ["10:00", "12:00", "14:00", "16:00", "18:00"],
          },
        ],
      },
    ],
  },
];

const MovieSchedule = ({ movieId }: { movieId: string }) => {
  const {
    region: userRegion,
    coordinates: userCoors,
    loading,
    error,
    handleActive,
  } = useLocation();
  const [selectedRegion, setSelectedRegion] = useState<Region | undefined>();
  const [selectedDate, setSelectedDate] = useState(scheduleDates[0]);

  const ref = query(
    collection(db, "cinemas").withConverter(cinemaConverter),
    where("region", "==", selectedRegion?.id || "")
  );
  const cinemasQuery = useFirestoreQuery(
    ["cinemas", selectedRegion?.id],
    ref,
    {},
    {
      enabled: !!selectedRegion,
    }
  );
  const snapshot = cinemasQuery.data;
  const cinemas = snapshot?.docs.map((doc) => ({
    ...doc.data(),
    id: doc.id,
  }));

  const scheduleRef = query(
    collection(db, "schedules").withConverter(scheduleConverter),
    where("movieId", "==", movieId),
    where("region", "==", selectedRegion?.id || ""),
    where("timestamp", "==", selectedDate),
    limit(1)
  );
  const scheduleQuery = useFirestoreQueryData(
    ["schedule", movieId, selectedRegion?.id, selectedDate],
    scheduleRef,
    {},
    {
      enabled: !!selectedRegion,
    }
  );
  const scheduleData = scheduleQuery?.data?.[0];

  useEffect(() => {
    if (error) {
      toast.error(error.message, toastOptions);
    }
  }, [error]);

  useEffect(() => {
    if (selectedRegion) {
      console.log(selectedRegion);
    }
  }, [selectedRegion]);

  useEffect(() => {
    if (userRegion) {
      setSelectedRegion({
        name: userRegion.name,
        id: userRegion.id,
      });
    }
  }, [userRegion]);
  let scheduleDetail;
  if (!selectedRegion) {
    scheduleDetail = (
      <p className="text-[17px] text-gray-light">Select your region</p>
    );
  } else if (scheduleQuery.isLoading || cinemasQuery.isLoading) {
    scheduleDetail = (
      <p className="text-[17px] text-gray-light">Finding schedules...</p>
    );
  } else if (!scheduleData || !cinemas) {
    scheduleDetail = (
      <p className="text-[17px] text-gray-light">No schedules found</p>
    );
  } else {
    console.log(scheduleData);
    scheduleDetail = (
      <ScheduleDetail
        coors={userCoors}
        data={scheduleData}
        cinemas={cinemas}
        date={selectedDate}
      />
    );
  }
  return (
    <div className="w-max flex flex-col gap-5">
      <ToastContainer />
      <div className="flex gap-4">
        <LoadingButton
          onClick={handleActive}
          loading={loading}
          svgIcon={LocationIcon}
          className="self-end"
        >
          Find nearest cinema
        </LoadingButton>
        <SelectLocation
          selectedRegion={selectedRegion}
          onChange={setSelectedRegion}
        />
      </div>
      <div className="flex gap-4 text-primary-dark">
        {scheduleDates.map((date) => (
          <ScheduleButton
            key={date}
            active={date === selectedDate}
            date={date}
            onClick={() => setSelectedDate(date)}
          />
        ))}
      </div>
      {scheduleDetail}
    </div>
  );
};

export default MovieSchedule;
