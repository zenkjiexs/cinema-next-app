import React, { memo, useMemo, useState } from "react";
import { Seat } from "../../types/movieBooking";
import { getSeatStatusStyle, getSeatTypeStyle } from "./seatStyleHelpers";
import X from "../../assets/MovieBooking/X.svg";

type Props = {
  seat: Seat;
  boxSize: number;
  onClick: (seat: Seat) => void;
};

const Seat = ({ seat, boxSize, onClick }: Props) => {
  const [isChecked, setIsChecked] = useState(false);

  const handleClick = () => {
    if (seat.status !== "AVAILABLE") return;
    onClick(seat);
    setIsChecked((prevState) => !prevState);
  };

  const seatTypeStyle = useMemo(() => getSeatTypeStyle(seat.type), [seat.type]);
  const seatStatusStyle = useMemo(
    () => getSeatStatusStyle(seat.status),
    [seat.status]
  );

  const checkedStyle = isChecked ? "bg-secondary-yellow" : "";
  return (
    <div
      onClick={handleClick}
      className={`flex justify-center items-center cursor-pointer rounded-sm ${seatTypeStyle} ${seatStatusStyle} ${checkedStyle}`}
      style={{
        height: `${boxSize}px `,
        width: `${boxSize}px`,
      }}
    >
      {seat.status === "UNAVAILABLE" ? (
        <X width="70%" height="70%" />
      ) : (
        <span
          className={`${
            boxSize < 30 ? "text-[12px]" : "text-base"
          } tracking-tighter ${isChecked ? "text-gray-dark" : ""}`}
        >
          {seat.id}
        </span>
      )}
    </div>
  );
};

export default memo(Seat);
