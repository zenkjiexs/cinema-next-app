import Image from "next/image";
import React, { useEffect, useState } from "react";
import { Seat } from "../../types/movieBooking";
import BookingSeat from "./BookingSeat";
import ClockIcon from "../../assets/MovieBooking/Clock.svg";
import ChooseSeat from "./ChooseSeat";
import { Movie } from "@/types/movie";
import { useAppSelector } from "@/redux/store";
import { toast, ToastContainer } from "react-toastify";
import { toastOptions } from "@/utils/toastConfig";
import useAuth from "@/hooks/useAuth";
import { useRouter } from "next/router";
import { collection, addDoc, doc, runTransaction } from "firebase/firestore";
import { db } from "firebaseConfig";
import { useFirestoreDocumentData } from "@react-query-firebase/firestore";
import {
  formatTimeString,
  getDayName,
  getMonthAndDayStr,
} from "@/utils/helpers";
import { roomConverter } from "@/utils/firebaseConverter/firebaseConverter";

// type Props = {
//   movieId: string;
//   timeStr: string;
//   cinemaId: string;
//   roomId: string;
// };

type SeatByType = {
  seatType: string;
  seats: Seat[];
};

const getSeatPrice = (seat: Seat) => {
  return seat.price ? seat.price : seat.type.price;
};

type Props = {
  movie: Movie;
};

const MovieBooking = ({ movie }: Props) => {
  const [seatsByType, setSeatsByType] = useState<SeatByType[]>([]);

  const [totalPrice, setTotalPrice] = useState<number>(0);
  const router = useRouter();
  const user = useAuth();
  const booking = useAppSelector((state) => state.booking);
  const { roomShiftPath } = booking;
  const roomShift = useFirestoreDocumentData(
    ["roomShift", booking.roomShiftPath],
    doc(db, booking.roomShiftPath || "roomShift/YMGdyJgUnvjBS2FcRDli"),
    {},
    {
      staleTime: 3000,
    }
  );
  console.log(roomShift);

  if (!booking.roomShiftPath) {
    router.push(`/movies/${movie.urlId}`);
  }

  const handleCheckout = async () => {
    if (!user) return;
    if (!roomShiftPath) return;
    try {
      await runTransaction(db, async (transaction) => {
        const roomShift = await transaction.get(
          doc(db, roomShiftPath).withConverter(roomConverter)
        );
        const seats = roomShift.data()?.seatData;
        if (!seats) return;
        const seatsToBook = seatsByType.flatMap((seat) => seat.seats);
        for (const seatToBook of seatsToBook) {
          const dbSeat = seats.find((seat) => seat.id === seatToBook.id);
          if (!dbSeat) throw new Error(`Không tìm thấy ghế ${seatToBook.id}`);
          if (dbSeat.status !== "AVAILABLE") {
            throw new Error(`Ghế ${seatToBook.id} không khả dụng`);
          }
          dbSeat.status = "RESERVED";
        }
        transaction.update(doc(db, roomShiftPath), { seatData: seats });
        console.log(booking);
        transaction.set(doc(collection(db, "orders")), {
          userId: user.data?.uid,
          movieId: movie.id,
          email: user.data?.email,
          seats: seatsByType,
          bookingInfo: booking,
        });
      });
      toast.success("Đặt vé thành công", toastOptions);
    } catch (err: any) {
      toast.error(err.message);
    } finally {
      setTimeout(() => {
        router.push("/");
      }, 2000);
      roomShift.refetch();
    }
  };
  const handleToggleSeat = (toggledSeat: Seat) => {
    if (toggledSeat.status !== "AVAILABLE") {
      return;
    }
    const seatType = toggledSeat.type.name;
    const typeIdx = seatsByType.findIndex((seat) => seat.seatType === seatType);
    // if seat type not available then add new type
    if (typeIdx === -1) {
      setSeatsByType([
        ...seatsByType,
        {
          seatType,
          seats: [toggledSeat],
        },
      ]);
      setTotalPrice(totalPrice + getSeatPrice(toggledSeat));
      return;
    }
    // check if seat is already selected
    const seats = seatsByType[typeIdx].seats;
    const seatIdx = seats.findIndex((seat) => seat.id === toggledSeat.id);

    // if seat is not exist add new seat else remove
    const newSeats = [...seats];
    if (seatIdx === -1) {
      newSeats.push(toggledSeat);
      setTotalPrice(totalPrice + getSeatPrice(toggledSeat));
    } else {
      newSeats.splice(seatIdx, 1);
      setTotalPrice(totalPrice - getSeatPrice(toggledSeat));
    }

    setSeatsByType(
      seatsByType.map((seatByType, idx) => {
        if (idx === typeIdx) {
          return {
            seatType,
            seats: newSeats,
          };
        }
        return seatByType;
      })
    );
  };
  console.log(seatsByType);
  const startTime = formatTimeString(new Date(booking.time || "2022-01-01"));
  const endTime = formatTimeString(new Date(booking.endTime || "2022-01-01"));

  return (
    <>
      <div className="w-5/6 mx-auto p-8 flex">
        <div className="flex w-[70%] flex-col gap-8 bg-title-dark p-12 rounded-l-2xl">
          <div className="flex justify-between">
            <div className="flex flex-col">
              <h3 className="text-2xl">
                <span className="font-bold">{startTime}, </span>
                {booking.cinemaName}
              </h3>
              <span className="uppercase">{booking.room}</span>
            </div>
            <div className="flex flex-col">
              <h3 className="text-2xl">
                <span className="font-bold">
                  {getDayName(booking.time || 1653797357, "en-US")}
                  {", "}
                </span>
                {getMonthAndDayStr(booking.time || 1653797357, "en-US")}
              </h3>
              <span className="self-end">
                {startTime} - {endTime}
              </span>
            </div>
          </div>

          <BookingSeat
            seatDatas={roomShift.data?.seatData}
            rowSize={roomShift.data?.rowSize}
            onToggleSeat={handleToggleSeat}
          />
        </div>
        <div className="flex flex-col flex-1 rounded-r-2xl overflow-hidden bg-[#2b2b2b] gap-8">
          <div className="relative w-full h-[500px]">
            <div className="w-full h-full bg-image-booking-layout absolute z-10"></div>
            <Image
              src={`${movie.imageUrl}`}
              alt="Movie Poster"
              layout="fill"
              objectFit="cover"
            />
            <div className="flex flex-col z-20 ml-8 gap-5 absolute bottom-0">
              <div className="flex gap-2">
                <ClockIcon width="20" height="20" />
                <span>{movie.duration}</span>
              </div>
              <div className="flex">
                <div className="flex flex-col">
                  <h2 className="text-2xl font-bold">{movie.title}</h2>
                  <div className="font-bold text-gray-light">
                    {movie.genres.join(", ")}
                  </div>
                  <div className="line-clamp-3 mt-5">{movie.description}</div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col flex-1 relative px-8 py-4 gap-8">
            <ChooseSeat seatsByType={seatsByType} />
            <button
              onClick={handleCheckout}
              className="btn bg-secondary-yellow text-primary-dark hover:bg-secondary-yellow flex justify-between px-6"
            >
              <span>Total ${totalPrice}</span>
              <span>Checkout</span>
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default MovieBooking;
