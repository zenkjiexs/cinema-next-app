import React, { useRef } from "react";
import { TransformComponent, TransformWrapper } from "react-zoom-pan-pinch";
import { useContainerDimensions } from "../../hooks/useContainerDimensions";
import { Seat, SeatType } from "../../types/movieBooking";
import SeatBox from "./SeatBox";
import X from "../../assets/MovieBooking/X.svg";
import ZoomIn from "../../assets/MovieBooking/ZoomIn.svg";
import { calculateSeatBoxWidth, getSeatTypeStyle } from "./seatStyleHelpers";
const maxSlot = 15;

// generate dummy data
const generateSeatData = (maxColumn: number, maxRow: number) => {
  const seatData: Seat[] = [];
  for (let i = 0; i < maxRow; i++) {
    const rowId = String.fromCharCode(65 + i);
    for (let j = 1; j <= maxColumn; j++) {
      const seat: Seat = {
        type: { name: "Standard", id: "standard", price: 70 },
        status: "AVAILABLE",
        id: `${rowId}${j}`,
      };
      seatData.push(seat);
    }
  }
  return seatData;
};

const seatData: Seat[] = generateSeatData(maxSlot, 12);
console.log(seatData);

const seatTypes: SeatType[] = [
  { name: "Standard", id: "standard", price: 70 },
  { name: "Vip", id: "vip", price: 100 },
  { name: "Couple", id: "couple", price: 200 },
  { name: "Deluxe", id: "deluxe", price: 180 },
];

type Props = {
  onToggleSeat: (seat: Seat) => void;
  seatDatas: Seat[] | undefined;
  rowSize: number | undefined;
};

const BookingSeat = ({ onToggleSeat, seatDatas, rowSize }: Props) => {
  const myRef = useRef<HTMLDivElement>(null);
  const { width } = useContainerDimensions(myRef);
  let gridGap = maxSlot >= 22 ? 1 : 2;

  const boxSize = calculateSeatBoxWidth(maxSlot, width, gridGap);
  return (
    <div ref={myRef} className="w-full flex flex-col">
      <div className="w-full h-10 bg-[#656565] rounded-lg flex justify-center items-center">
        <span className="text-xl">MONITOR</span>
      </div>
      <div className="w-full flex flex-col items-center py-8 gap-4">
        <p className="flex gap-1 items-center">
          <ZoomIn width={18} height={18} /> Zoom in and move around
        </p>
        <TransformWrapper
          initialScale={0.85}
          minScale={0.7}
          maxScale={2}
          centerOnInit={true}
          doubleClick={{ disabled: true }}
          panning={{
            velocityDisabled: true,
          }}
        >
          {() => (
            <TransformComponent>
              <div
                className={`w-full gap-${gridGap} grid justify-center`}
                style={{
                  gridTemplateColumns: `repeat(${
                    rowSize || maxSlot
                  }, ${boxSize}px)`,
                }}
              >
                {seatDatas &&
                  seatDatas.map((seat, idx) => {
                    return (
                      <SeatBox
                        key={idx}
                        seat={seat}
                        boxSize={boxSize}
                        onClick={onToggleSeat}
                      />
                    );
                  })}
              </div>
            </TransformComponent>
          )}
        </TransformWrapper>
        <div className="grid grid-rows-3 grid-flow-col gap-y-2 gap-x-6">
          <div className="flex gap-3 items-center">
            <div className="w-6 h-6 rounded-sm bg-secondary-yellow "></div>
            <span className="uppercase text-sm">Checked</span>
          </div>
          <div className="flex gap-3 items-center">
            <div className="w-6 h-6 rounded-sm bg-gray-light"></div>
            <span className="uppercase text-sm">Reserved</span>
          </div>
          <div className="flex gap-3 items-center">
            <div className="w-6 h-6 rounded-sm bg-gray-light flex items-center justify-center">
              <X width="70%" height="70%" />
            </div>
            <span className="uppercase text-sm">Unavailable</span>
          </div>

          {seatTypes.map((seatType, idx) => {
            const seatTypeStyle = getSeatTypeStyle(seatType);
            return (
              <div key={idx} className="flex gap-3 items-center">
                <div className={`w-6 h-6 rounded-sm ${seatTypeStyle}`}></div>
                <span className="uppercase text-sm">{seatType.name}</span>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default BookingSeat;
