import { Rating } from "@/types/movie";
import { ratingConverter } from "@/utils/firebaseConverter/firebaseConverter";
import {
  useFirestoreDocument,
  useFirestoreDocumentData,
} from "@react-query-firebase/firestore";
import { doc, DocumentReference } from "firebase/firestore";
import { db } from "firebaseConfig";
import Image from "next/image";
import React, { FC } from "react";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import GroupIcon from "/assets/MovieList/Group.svg";
import StarIcon from "/assets/MovieList/Star.svg";
type Props = {
  title: string;
  imageUrl: string;
  duration: number;
  genres: string[];
  isLoading?: boolean;
  ratingPath: string;
  id: string;
  onClick: () => void;
};

const MovieCard: FC<Props> = ({
  id,
  title,
  imageUrl,
  duration,
  genres,
  isLoading = false,
  ratingPath,
  onClick,
}) => {
  const durationString = `${Math.floor(duration / 60)}h ${duration % 60}m`;
  const ref = doc(db, ratingPath).withConverter(ratingConverter);
  const ratingData = useFirestoreDocumentData(["ratings", id], ref);

  return (
    <div
      onClick={onClick}
      className="w-full h-full relative rounded-md overflow-hidden cursor-pointer group"
    >
      {isLoading ? (
        <SkeletonTheme baseColor="#777777" highlightColor="#444">
          <Skeleton />
        </SkeletonTheme>
      ) : (
        <>
          <div className="w-full h-full relative transition-all scale-100 group-hover:scale-110 flex justify-center">
            <Image
              src={imageUrl}
              alt={`${title} cover`}
              layout="fill"
              objectFit="cover"
            />
          </div>
          <div className="w-full h-full absolute top-0 left-0 bg-image-layout group-hover:bg-image-layout-dark z-10"></div>
          <div className="h-full flex flex-col justify-between items-start absolute top-0 z-20 px-6 py-6">
            <div className="flex gap-3 flex-wrap">
              {genres.map((genre, idx) => (
                <div
                  key={idx}
                  className="md:min-w-16 px-3 text-center bg-secondary-yellow text-title-dark rounded-2xl"
                >
                  {genre}
                </div>
              ))}
            </div>
            <div className="flex flex-col w-full">
              <div className="flex gap-10 mb-7">
                <div className="flex gap-2 items-center">
                  <GroupIcon width="20" height="20" />
                  <span>{durationString}</span>
                </div>
                <div className="flex gap-2 items-center">
                  <StarIcon width="20" height="20" />
                  <span>{ratingData.data?.rating || "5.0"}</span>
                </div>
              </div>
              <h2 className="text-4xl font-bold">{title}</h2>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default MovieCard;
