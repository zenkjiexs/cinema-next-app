import Image from "next/image";
import React from "react";
import ArrowRight from "/assets/Footer/ArrowRight.svg";
import Location from "/assets/Footer/Location.svg";
import Phone from "/assets/Footer/Phone.svg";
import Mail from "/assets/Footer/Mail.svg";
type FooterItem = {
  name: string;
};

type FooterCol = {
  title: string;
  items: FooterItem[];
};

const footerData: FooterCol[] = [
  {
    title: "Movies",
    items: [{ name: "Now Showing" }, { name: "Upcoming" }, { name: "Trailer" }],
  },
  {
    title: "About",
    items: [{ name: "About Us" }, { name: "Contact Us" }, { name: "Careers" }],
  },
  {
    title: "Help",
    items: [
      { name: "Help Center" },
      { name: "FAQ" },
      { name: "Terms & Conditions" },
    ],
  },
];

const Footer = () => {
  return (
    <div className="w-full flex">
      <div className="flex flex-col basis-2/5 gap-8 bg-[#3D3D3D] px-24 py-20">
        <h2 className="text-5xl font-bold">Watchflix</h2>
        <p className="leading-8">
          Get the best news and promotions about movies in your inbox every day.
          Don&apos;t miss out on the latest movies and TV shows.
        </p>
        <div className="flex flex-col gap-4">
          <span className="font-bold">Join NewsLetters</span>
          <div className="w-3/4 flex gap-2 items-center bg-[#525252] rounded-md py-2 px-3">
            <input
              className="w-full outline-none bg-transparent"
              type="text"
              placeholder="Enter your email"
            />
            <button className="bg-secondary-yellow rounded-md p-2 flex items-center justify-center">
              <ArrowRight />
            </button>
          </div>
        </div>
      </div>
      <div className="flex-1 bg-title-dark px-16 py-24 flex flex-col justify-between">
        <div className="flex gap-40">
          {footerData.map((col, index) => (
            <div key={index}>
              <h4 className="font-bold mb-6">{col.title}</h4>
              <ul className="flex flex-col gap-6">
                {col.items.map((item, index) => (
                  <li key={index} className="cursor-pointer">
                    <span>{item.name}</span>
                  </li>
                ))}
              </ul>
            </div>
          ))}
        </div>
        <div className="flex gap-8">
          <div className="flex gap-2">
            <Location />
            <span>Ha Dong, Hanoi, Vietnam</span>
          </div>
          <div className="flex gap-2">
            <Mail />
            <span>tuanxsokoh@gmail.com</span>
          </div>
          <div className="flex gap-2">
            <Phone />
            <span>+84 987 654 321</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default React.memo(Footer);
