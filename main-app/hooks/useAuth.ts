import { MyUserData } from "@/types/profile";
import { objectNotEmpty } from "@/utils/helpers";
import { useAuthUser } from "@react-query-firebase/auth";
import { AuthError, User } from "firebase/auth";
import { auth } from "firebaseConfig";
import { useEffect, useState } from "react";
import { QueryKey, UseQueryOptions } from "react-query";
import useUserData from "./useUserData";

const useAuth = (
  useQueryOptions?: Omit<
    UseQueryOptions<User | null, AuthError, User | null, QueryKey>,
    "queryFn"
  >
) => {
  const authUser = useAuthUser("authUser", auth, {
    ...useQueryOptions,
  });
  const [data, setData] = useState<MyUserData | undefined>();
  const userDb = useUserData(auth);
  useEffect(() => {
    if (!userDb.isSuccess || !authUser.isSuccess) {
      return;
    }

    if (
      data?.updatedAt !== userDb.data?.updatedAt ||
      data?.photoURL !== authUser.data?.photoURL
    ) {
      if (!objectNotEmpty(userDb.data) && !objectNotEmpty(authUser.data)) {
        setData(undefined);
        return;
      }

      const newData = {
        ...userDb.data,
        ...authUser.data,
      } as MyUserData;
      setData(newData);
    }
  }, [userDb, authUser, data]);

  const remove = () => {
    authUser.remove();
    userDb.remove();
  };

  const refetch = () => {
    authUser.refetch();
    userDb.refetch();
  };

  return { ...authUser, remove, data, refetch };
};

export default useAuth;
