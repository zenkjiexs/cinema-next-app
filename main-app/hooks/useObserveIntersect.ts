import { RefObject, useEffect, useMemo, useState } from "react";
type ObserveOptions = {
  root?: HTMLElement | null;
  rootMargin?: string;
  threshold?: number | number[];
};

const useObserveIntersect = (
  options: ObserveOptions,
  targetRef: RefObject<HTMLElement> | undefined
) => {
  const [isIntersect, setIntersect] = useState(false);

  const callbackFn = (entries: any) => {
    const [entry] = entries;
    setIntersect(entry.isIntersecting);
  };
  const myOptions = useMemo(() => options, [options]);

  useEffect(() => {
    const observer = new IntersectionObserver(callbackFn, myOptions);
    const currentTarget = targetRef?.current;
    if (currentTarget) {
      observer.observe(currentTarget);
    }
    return () => {
      if (currentTarget) {
        observer.unobserve(currentTarget);
      }
    };
  }, [targetRef, myOptions]);
  return isIntersect;
};

export default useObserveIntersect;
