import { useEffect, useReducer, useState } from "react";
import { getUserRegion } from "../utils/api/location";
import {
  formatRegionId,
  getWithExpiration,
  storeWithExpiration,
} from "../utils/helpers";

type Coordinates = {
  latitude: number;
  longitude: number;
};

type Region = {
  name: string;
  id: string;
};

enum ActionKind {
  SET_USER_COORS = "SET_USER_COORS",
  SET_USER_REGION = "SET_USER_REGION",
  LOADING = "LOADING",
  ERROR = "ERROR",
}

type Error = {
  message: string;
};

type Action =
  | { type: "SET_USER_COORS"; payload: Coordinates }
  | { type: "SET_USER_REGION"; payload: Region }
  | { type: "LOADING" }
  | { type: "ERROR"; payload: Error | undefined };

type LocationState = {
  region: Region | undefined;
  coordinates: Coordinates | undefined;
  loading: boolean;
  error: Error | undefined;
};

const initialState = {
  coordinates: undefined,
  region: undefined,
  loading: false,
  error: undefined,
};

type cacheLocation = Coordinates & Region;

const locationReducer = (state: LocationState, action: Action) => {
  switch (action.type) {
    case ActionKind.SET_USER_COORS:
      return {
        ...state,
        loading: false,
        coordinates: action.payload,
      };
    case ActionKind.SET_USER_REGION:
      return {
        ...state,
        loading: false,
        region: action.payload,
      };
    case ActionKind.LOADING:
      return {
        ...state,
        loading: true,
      };
    case ActionKind.ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

const dispatchDeplay = (dispatch: (action: Action) => void, action: Action) => {
  setTimeout(() => {
    dispatch(action);
  }, 1000);
};

const useLocation = () => {
  const [state, dispatch] = useReducer(locationReducer, initialState);

  const handleActive = () => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;

        dispatch({
          type: ActionKind.SET_USER_COORS,
          payload: { latitude, longitude },
        });
      },
      (error) => {
        dispatch({
          type: ActionKind.ERROR,
          payload: { message: "Please enable location services" },
        });
      },
      {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0,
      }
    );
  };
  const { coordinates, region, loading, error } = state;

  useEffect(() => {
    if (!coordinates) {
      return;
    }
    const { latitude, longitude } = coordinates;

    dispatch({ type: ActionKind.LOADING });
    const cachedLocation = getWithExpiration("location");
    if (cachedLocation) {
      const { coordinates: cachedCoors, userRegion: cachedRegion } =
        cachedLocation;
      if (
        cachedCoors.latitude === latitude &&
        cachedCoors.longitude === longitude
      ) {
        dispatchDeplay(dispatch, {
          type: ActionKind.SET_USER_REGION,
          payload: cachedRegion,
        });
        return;
      }
    }

    getUserRegion({ latitude, longitude })
      .then((regionName: string) => {
        const userRegion: Region = {
          id: formatRegionId(regionName),
          name: regionName,
        };
        dispatchDeplay(dispatch, {
          type: ActionKind.SET_USER_REGION,
          payload: userRegion,
        });
        storeWithExpiration(
          "location",
          {
            userRegion,
            coordinates: { latitude, longitude },
          },
          3600
        );
      })
      .catch(() => {
        dispatchDeplay(dispatch, {
          type: ActionKind.ERROR,
          payload: { message: "Unable to get your location" },
        });
      });
  }, [coordinates]);
  return { handleActive, region, loading, error, coordinates };
};

export default useLocation;
