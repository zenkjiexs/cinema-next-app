module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        "primary-dark": "#000000",
        "secondary-dark": "#1E1E1E",
        "title-dark": "#2E2E2E",
        "field-bg": "#222222",
        "typo-light": "#ffffff",
        "secondary-yellow": "#FED530",
        "text-infield": "#ccc",
        "light-icon": "#9A9AB0",
        "gray-light": "#8f8f8f",
        "gray-dark": "#3d3d3d",
      },
      backgroundImage: {
        "image-layout":
          "linear-gradient(180deg, rgba(0,0,0,0) 0%, rgba(0,0,0,1) 100%)",
        "image-layout-dark": "linear-gradient(0deg,#000,transparent)",
        "hero-layout":
          "linear-gradient(90deg, rgba(0,0,0,0) 0%, rgba(0,0,0,1) 100%)",
        "image-booking-layout":
          "linear-gradient(0deg, hsla(0, 0%, 17%, 1) 5%, hsla(0, 0%, 100%, 0) 100%)",
        "auth-bg": "linear-gradient(315deg, #485461 0%, #28313b 74%)",
        "auth-btn": "linear-gradient(to right, #e65c00, #f9d423)",
      },
      boxShadow: {
        "light-thin": "rgba(0, 0, 0, 0.24) 0px 3px 8px",
        "dark-thin": "rgba(0, 0, 0, 0.35) 0px 5px 15px",
      },

      fontFamily: {
        sans: ["DM Sans", "sans-serif"],
      },
      animation: {
        "bubble-slow": "bubble 2s ease-in-out infinite",
      },
      keyframes: {
        bubble: {
          "0%": {
            transform: "scale(1)",
          },
          "50%": {
            transform: "scale(1.15)",
          },
          "100%": {
            transform: "scale(1)",
          },
        },
      },
    },
  },
  plugins: [require("daisyui"), require("@tailwindcss/line-clamp")],
};
