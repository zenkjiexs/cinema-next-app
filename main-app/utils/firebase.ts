import { User } from "firebase/auth";
import { doc, getDoc, setDoc } from "firebase/firestore";
import { db } from "firebaseConfig";

export const createUserIfNotExists = async (data: User) => {
  const ref = doc(db, "users", data.uid);
  const document = await getDoc(ref);
  if (document.exists()) return;
  setDoc(ref, {
    uid: data.uid,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  });
};

export const replaceUser = async (data: User, customDisplayName?: string) => {
  const ref = doc(db, "users", data.uid);

  setDoc(ref, {
    uid: data.uid,
    email: data.email,
    displayName: data.displayName || customDisplayName,
    photoURL: data.photoURL,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  });
};

export const getUser = async (uid: string) => {
  const ref = doc(db, "users", uid);
  const document = await getDoc(ref);
  return document.data();
};
