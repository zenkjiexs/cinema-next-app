import * as yup from "yup";

//REGEX for password: Minimum eight characters, at least one letter and one number
const REGEX_PASSWORD = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;

yup.addMethod(yup.string, "password", function (message: string) {
  return this.matches(REGEX_PASSWORD, {
    message,
    excludeEmptyString: true,
  });
});

export default yup;
