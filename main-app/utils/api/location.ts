import axios from "axios";

interface Coordinates {
  latitude: number;
  longitude: number;
}

interface LocationOptions extends Coordinates {}

const LOCATION_REVERSE_URL = `https://geocode.xyz`;

export const getUserRegion = async ({
  latitude: lat,
  longitude: lng,
}: Coordinates) => {
  const { data } = await axios.get(
    `${LOCATION_REVERSE_URL}/${lat},${lng}?json=1`
  );
  return data.region;
};
