import { createSlice } from "@reduxjs/toolkit";

type NavState = {
  scrollToElement: string | undefined;
};

const initialState: NavState = {
  scrollToElement: undefined,
};

const navSlice = createSlice({
  name: "nav",
  initialState,
  reducers: {
    scrollToElement: (state, action) => {
      state.scrollToElement = action.payload;
    },
  },
});
export const navActions = navSlice.actions;
export default navSlice.reducer;
