import { createSlice } from "@reduxjs/toolkit";

type initState = {
  time: number | undefined;
  endTime: number | undefined;
  type: string | undefined;
  cinemaId: string | undefined;
  room: string | undefined;
  roomShiftPath: string | undefined;
  cinemaName: string | undefined;
};

const initialState: initState = {
  time: undefined,
  endTime: undefined,
  type: undefined,
  cinemaId: undefined,
  room: undefined,
  roomShiftPath: undefined,
  cinemaName: undefined,
};

const bookingSlice = createSlice({
  name: "nav",
  initialState,
  reducers: {
    booking: (state, action) => {
      state.time = action.payload.time;
      state.type = action.payload.type;
      state.cinemaId = action.payload.cinemaId;
      state.room = action.payload.room;
      state.roomShiftPath = action.payload.roomShiftPath;
      state.endTime = action.payload.endTime;
      state.cinemaName = action.payload.cinemaName;
    },
  },
});
export const bookingActions = bookingSlice.actions;
export default bookingSlice.reducer;
