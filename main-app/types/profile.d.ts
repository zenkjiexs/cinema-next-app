import { User } from "firebase/auth";

export type Tab = {
  name: string;
  code: string;
};

export type UserData = {
  uid: string;
  createdAt: number;
  updatedAt: number;
  address?: string;
  phone?: string;
};

interface MyUserData extends Omit<UserData, "uid">, User {}
