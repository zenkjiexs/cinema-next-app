export type AuthFormValues = {
  fullName: string;
  email: string;
  password: string;
};

export type ProfileForm = {
  fullName: string;
  email: string;
  address: string;
  phone: string;
};

export type ChangePasswordForm = {
  curPassword: string;
  newPassword: string;
  confirmPassword: string;
};
